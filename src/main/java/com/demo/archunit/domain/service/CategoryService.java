package com.demo.archunit.domain.service;

import com.demo.archunit.domain.exception.DuplicateDataException;
import com.demo.archunit.domain.model.Category;
import com.demo.archunit.domain.port.CategoryRepositoryPort;
import org.springframework.stereotype.Service;

public class CategoryService {

	public static final String MESSAGE_DUPLICATE_ERROR = "Existing category with name {0}";

	private CategoryRepositoryPort categoryRepositoryPort;

	public CategoryService(CategoryRepositoryPort categoryRepositoryPort) {
		this.categoryRepositoryPort = categoryRepositoryPort;
	}

	public void create(Category category) {
		Category existCategory = categoryRepositoryPort.findByName(category.getName());
		if (existCategory != null) {
			throw new DuplicateDataException(MESSAGE_DUPLICATE_ERROR, category.getName());
		}
		categoryRepositoryPort.save(category);
	}
}
