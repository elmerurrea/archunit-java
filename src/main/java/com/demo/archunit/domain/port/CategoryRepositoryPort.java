package com.demo.archunit.domain.port;

import com.demo.archunit.domain.model.Category;
import java.util.List;

public interface CategoryRepositoryPort {

	List<Category> findAll();

	Category findByName(String name);

	void save(Category category);

}
