package com.demo.archunit.domain.port;

import com.demo.archunit.domain.model.Product;
import java.util.List;

public interface ProductRepositoryPort {

	List<Product> findAll();

	List<Product> findByCategory(String categoryName);

}
