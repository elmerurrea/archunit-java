package com.demo.archunit.domain.exception;

import java.text.MessageFormat;

public class DuplicateDataException extends RuntimeException {

	public DuplicateDataException(String message) {
		super(message);
	}

	public DuplicateDataException(String message, Object... params) {
		super(MessageFormat.format(message, params));
	}

}
