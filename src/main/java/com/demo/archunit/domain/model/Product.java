package com.demo.archunit.domain.model;

import lombok.Data;

@Data
public class Product {

	private long id;
	private String name;
	private String description;
	private Category category;
	private Double price;

}
