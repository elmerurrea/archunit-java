package com.demo.archunit.infrastructure.adapter;

import com.demo.archunit.domain.model.Product;
import com.demo.archunit.domain.port.ProductRepositoryPort;
import com.demo.archunit.infrastructure.adapter.entity.ProductEntity;
import com.demo.archunit.infrastructure.adapter.mapper.ProductMapper;
import com.demo.archunit.infrastructure.adapter.repository.ProductRepositoryJpa;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ProductRepositoryAdapter implements ProductRepositoryPort {

	private ProductRepositoryJpa productRepositoryJpa;
	private ProductMapper productMapper;

	public ProductRepositoryAdapter(ProductRepositoryJpa productRepositoryJpa,
		ProductMapper productMapper) {
		this.productRepositoryJpa = productRepositoryJpa;
		this.productMapper = productMapper;
	}

	@Override
	public List<Product> findAll() {
		return productMapper.listToDomain((List<ProductEntity>) productRepositoryJpa.findAll());
	}

	@Override
	public List<Product> findByCategory(String categoryName) {
		return productMapper.listToDomain(productRepositoryJpa.findByCategory_name(categoryName));
	}
}
