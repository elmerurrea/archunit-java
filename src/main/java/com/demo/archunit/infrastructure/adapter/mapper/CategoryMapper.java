package com.demo.archunit.infrastructure.adapter.mapper;

import com.demo.archunit.domain.model.Category;
import com.demo.archunit.infrastructure.adapter.entity.CategoryEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CategoryMapper {

	CategoryEntity toEntity(Category category);

	Category toDomain(CategoryEntity categoryEntity);

	List<Category> listToDomain(List<CategoryEntity> categoryEntity);

}
