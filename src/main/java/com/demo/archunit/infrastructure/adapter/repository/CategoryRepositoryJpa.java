package com.demo.archunit.infrastructure.adapter.repository;

import com.demo.archunit.infrastructure.adapter.entity.CategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepositoryJpa extends CrudRepository<CategoryEntity, Long> {

	CategoryEntity findByName(String name);

}
