package com.demo.archunit.infrastructure.adapter.repository;

import com.demo.archunit.infrastructure.adapter.entity.ProductEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepositoryJpa extends CrudRepository<ProductEntity, Long> {

	List<ProductEntity> findByCategory_name(String category);

}
