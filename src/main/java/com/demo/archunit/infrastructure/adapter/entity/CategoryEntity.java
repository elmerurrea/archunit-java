package com.demo.archunit.infrastructure.adapter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Data
@Entity(name = "category")
public class CategoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CATEGORY")
	@SequenceGenerator(name = "SQ_CATEGORY", sequenceName = "SQ_CATEGORY", allocationSize = 1)
	@Column(name = "id_category")
	private long id;

	@Column(nullable = false)
	private String name;

	private String description;

}
