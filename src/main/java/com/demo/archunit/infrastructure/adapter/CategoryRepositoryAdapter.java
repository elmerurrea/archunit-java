package com.demo.archunit.infrastructure.adapter;

import com.demo.archunit.domain.model.Category;
import com.demo.archunit.domain.port.CategoryRepositoryPort;
import com.demo.archunit.infrastructure.adapter.entity.CategoryEntity;
import com.demo.archunit.infrastructure.adapter.mapper.CategoryMapper;
import com.demo.archunit.infrastructure.adapter.repository.CategoryRepositoryJpa;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CategoryRepositoryAdapter implements CategoryRepositoryPort {

	private CategoryRepositoryJpa categoryRepositoryJpa;
	private CategoryMapper categoryMapper;

	public CategoryRepositoryAdapter(CategoryRepositoryJpa categoryRepositoryJpa,
		CategoryMapper categoryMapper) {
		this.categoryRepositoryJpa = categoryRepositoryJpa;
		this.categoryMapper = categoryMapper;
	}

	@Override
	public List<Category> findAll() {
		return categoryMapper.listToDomain((List<CategoryEntity>) categoryRepositoryJpa.findAll());
	}

	@Override
	public Category findByName(String name) {
		return categoryMapper.toDomain(categoryRepositoryJpa.findByName(name));
	}

	@Override
	public void save(Category category) {
		categoryRepositoryJpa.save(categoryMapper.toEntity(category));
	}
}
