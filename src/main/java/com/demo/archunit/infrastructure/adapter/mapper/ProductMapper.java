package com.demo.archunit.infrastructure.adapter.mapper;

import com.demo.archunit.domain.model.Product;
import com.demo.archunit.infrastructure.adapter.entity.ProductEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = CategoryMapper.class)
public interface ProductMapper {

	List<Product> listToDomain(List<ProductEntity> productEntity);

}
