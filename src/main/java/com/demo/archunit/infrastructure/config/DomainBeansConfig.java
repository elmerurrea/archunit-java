package com.demo.archunit.infrastructure.config;

import com.demo.archunit.domain.port.CategoryRepositoryPort;
import com.demo.archunit.domain.service.CategoryService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainBeansConfig {

	@Bean
	public CategoryService categoryService(CategoryRepositoryPort categoryRepositoryPort) {
		return new CategoryService(categoryRepositoryPort);
	}

}
