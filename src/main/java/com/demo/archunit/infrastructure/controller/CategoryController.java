package com.demo.archunit.infrastructure.controller;

import com.demo.archunit.application.dto.CategoryDto;
import com.demo.archunit.application.service.CategoryApp;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("categories")
public class CategoryController {

	private CategoryApp categoryApp;

	public CategoryController(CategoryApp categoryApp) {
		this.categoryApp = categoryApp;
	}

	@GetMapping
	public List<CategoryDto> findAll() {
		return categoryApp.findAll();
	}

	@PostMapping
	public void create(@RequestBody CategoryDto categoryDto) {
		categoryApp.create(categoryDto);
	}
}
