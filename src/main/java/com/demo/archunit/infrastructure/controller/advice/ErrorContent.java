package com.demo.archunit.infrastructure.controller.advice;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorContent {

	private String name;
	private String message;

}
