package com.demo.archunit.infrastructure.controller.advice;

import com.demo.archunit.domain.exception.DuplicateDataException;
import com.demo.archunit.domain.exception.RequiredValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AdviceController extends ResponseEntityExceptionHandler {

	private static final String MESSAGE_UNEXPECTED_EXCEPTION = "Unexpected exception.";

	@ExceptionHandler(RequiredValueException.class)
	ResponseEntity<ErrorContent> handleBadRequestException(RequiredValueException exception) {
		return new ResponseEntity(
			new ErrorContent(exception.getClass().getSimpleName(), exception.getMessage()),
			HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DuplicateDataException.class)
	ResponseEntity<ErrorContent> handleConflictException(DuplicateDataException exception) {
		return new ResponseEntity(
			new ErrorContent(exception.getClass().getSimpleName(), exception.getMessage()),
			HttpStatus.CONFLICT);
	}

	@ExceptionHandler(Exception.class)
	ResponseEntity<ErrorContent> handleUnexpectedException(Exception exception) {
		return new ResponseEntity(
			new ErrorContent("Unexpected", MESSAGE_UNEXPECTED_EXCEPTION),
			HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
