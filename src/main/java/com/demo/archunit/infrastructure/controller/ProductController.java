package com.demo.archunit.infrastructure.controller;

import com.demo.archunit.application.service.ProductAppService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("products")
public class ProductController {

	private ProductAppService productAppService;

	public ProductController(ProductAppService productAppService) {
		this.productAppService = productAppService;
	}
}
