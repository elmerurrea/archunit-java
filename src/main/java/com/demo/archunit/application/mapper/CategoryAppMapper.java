package com.demo.archunit.application.mapper;

import com.demo.archunit.application.dto.CategoryDto;
import com.demo.archunit.domain.model.Category;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class CategoryAppMapper {

	public CategoryDto toDto(Category category) {
		return new CategoryDto();
	}

	public Category toDomain(CategoryDto categoryDto) {
		return Category.builder()
			.id(categoryDto.getId())
			.name(categoryDto.getName())
			.description(categoryDto.getDescription())
			.build();
	}

	public List<CategoryDto> listToDto(List<Category> categories) {
		return categories.stream().map(this::toDto).collect(Collectors.toList());
	}

}
