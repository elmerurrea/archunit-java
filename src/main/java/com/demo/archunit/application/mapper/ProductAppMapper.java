package com.demo.archunit.application.mapper;

import com.demo.archunit.application.dto.ProductDto;
import com.demo.archunit.domain.model.Product;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ProductAppMapper {

	private CategoryAppMapper categoryAppMapper;

	public ProductAppMapper(CategoryAppMapper categoryAppMapper) {
		this.categoryAppMapper = categoryAppMapper;
	}

	public ProductDto toDto(Product product) {
		return ProductDto.builder()
			.id(product.getId())
			.name(product.getName())
			.description(product.getDescription())
			.category(categoryAppMapper.toDto(product.getCategory()))
			.price(product.getPrice())
			.build();
	}

	public List<ProductDto> listToDto(List<Product> products) {
		return products.stream().map(this::toDto).collect(Collectors.toList());
	}

}
