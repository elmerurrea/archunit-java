package com.demo.archunit.application.service;

import com.demo.archunit.application.dto.CategoryDto;
import com.demo.archunit.application.mapper.CategoryAppMapper;
import com.demo.archunit.domain.port.CategoryRepositoryPort;
import com.demo.archunit.domain.service.CategoryService;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CategoryApp {

	private CategoryAppMapper categoryAppMapper;
	private CategoryRepositoryPort categoryRepositoryPort;
	private CategoryService categoryService;

	public CategoryApp(CategoryAppMapper categoryAppMapper,
		CategoryRepositoryPort categoryRepositoryPort,
		CategoryService categoryService) {
		this.categoryAppMapper = categoryAppMapper;
		this.categoryRepositoryPort = categoryRepositoryPort;
		this.categoryService = categoryService;
	}

	public List<CategoryDto> findAll() {
		return categoryAppMapper.listToDto(categoryRepositoryPort.findAll());
	}

	public void create(CategoryDto categoryDto) {
		categoryService.create(categoryAppMapper.toDomain(categoryDto));
	}
}
