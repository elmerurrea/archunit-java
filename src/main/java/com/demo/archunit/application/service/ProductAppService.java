package com.demo.archunit.application.service;

import com.demo.archunit.application.dto.ProductDto;
import com.demo.archunit.application.mapper.ProductAppMapper;
import com.demo.archunit.domain.port.ProductRepositoryPort;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ProductAppService {

	private ProductAppMapper productAppMapper;
	private ProductRepositoryPort productRepositoryPort;

	public ProductAppService(ProductAppMapper productAppMapper,
		ProductRepositoryPort productRepositoryPort) {
		this.productAppMapper = productAppMapper;
		this.productRepositoryPort = productRepositoryPort;
	}

	public List<ProductDto> findAll() {
		return productAppMapper.listToDto(productRepositoryPort.findAll());
	}
}
