package com.demo.archunit.application.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {

	private long id;
	private String name;
	private String description;
	private CategoryDto category;
	private Double price;

}
