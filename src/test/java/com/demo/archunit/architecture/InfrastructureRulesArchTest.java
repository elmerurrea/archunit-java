package com.demo.archunit.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

@AnalyzeClasses(packages = "com.demo.archunit.infrastructure")
public class InfrastructureRulesArchTest {

	@ArchTest
	public void files_infrastructure_controller_folder_should_ending_with_Controller(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..controller").should()
			.haveSimpleNameEndingWith("Controller").check(javaClasses);
	}

	@ArchTest
	public void files_infrastructure_config_folder_should_ending_with_Config(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..config..").should()
			.haveSimpleNameEndingWith("Config").check(javaClasses);
	}

	@ArchTest
	public void files_infrastructure_adapter_folder_should_ending_with_RepositoryAdapter(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..adapter").should()
			.haveSimpleNameEndingWith("RepositoryAdapter").check(javaClasses);
	}

	@ArchTest
	public void files_infrastructure_entity_repository_folder_should_ending_with_Entity(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..entity..").should()
			.haveSimpleNameEndingWith("Entity").check(javaClasses);
	}

	@ArchTest
	public void files_infrastructure_mapper_folder_should_ending_with_Mapper(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..mapper..").should()
			.haveSimpleNameContaining("Mapper").check(javaClasses);
	}

	@ArchTest
	public void files_infrastructure_repository_folder_should_ending_with_RepositoryJpa(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..repository..").should()
			.haveSimpleNameEndingWith("RepositoryJpa").check(javaClasses);
	}

}
