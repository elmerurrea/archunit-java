package com.demo.archunit.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

@AnalyzeClasses(packages = "com.demo.archunit.domain")
public class DomainRulesArchTest {

	@ArchTest
	public void files_domain_service_folder_should_ending_with_Service(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..service..").should()
			.haveSimpleNameEndingWith("Service").check(javaClasses);
	}

	@ArchTest
	public void files_domain_port_folder_should_ending_with_Port(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..port..").should()
			.haveSimpleNameEndingWith("Port").check(javaClasses);
	}

}
