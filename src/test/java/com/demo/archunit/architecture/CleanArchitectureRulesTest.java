package com.demo.archunit.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.library.Architectures;

@AnalyzeClasses(packages = "com.demo.archunit")
public class CleanArchitectureRulesTest {

	private static final String DOMAIN_LAYER = "domain";
	private static final String APPLICATION_LAYER = "application";
	private static final String INFRASTRUCTURE_LAYER = "infrastructure";
	private static final String DOMAIN_PACKAGE = "com.demo.archunit.domain..";
	private static final String APPLICATION_PACKAGE = "com.demo.archunit.application..";
	private static final String INFRASTRUCTURE_PACKAGE = "com.demo.archunit.infrastructure..";
	private static final String JAVA_SOURCES_PACKAGE = "java..";

	@ArchTest
	public void hexagonal_layer_should_protect_clean_standards(JavaClasses javaClasses) {
		Architectures.layeredArchitecture()
			.layer(DOMAIN_LAYER).definedBy(DOMAIN_PACKAGE)
			.layer(APPLICATION_LAYER).definedBy(APPLICATION_PACKAGE)
			.layer(INFRASTRUCTURE_LAYER).definedBy(INFRASTRUCTURE_PACKAGE)
			.whereLayer(INFRASTRUCTURE_LAYER).mayNotBeAccessedByAnyLayer()
			.whereLayer(APPLICATION_LAYER).mayOnlyBeAccessedByLayers(INFRASTRUCTURE_LAYER)
			.whereLayer(DOMAIN_LAYER).mayOnlyBeAccessedByLayers(INFRASTRUCTURE_LAYER
			, APPLICATION_LAYER)
			.check(javaClasses);
	}

	@ArchTest
	public void domain_should_not_has_external_libraries(JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage(DOMAIN_PACKAGE)
			.should().onlyDependOnClassesThat()
			.resideInAnyPackage(JAVA_SOURCES_PACKAGE, DOMAIN_PACKAGE).check(javaClasses);
	}


}
