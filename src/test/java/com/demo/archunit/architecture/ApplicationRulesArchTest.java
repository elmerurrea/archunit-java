package com.demo.archunit.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

@AnalyzeClasses(packages = "com.demo.archunit.application")
public class ApplicationRulesArchTest {

	@ArchTest
	public void files_application_service_folder_should_ending_with_AppService(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..service..").should()
			.haveSimpleNameEndingWith("AppService").check(javaClasses);
	}

	@ArchTest
	public void files_application_mapper_folder_should_ending_with_AppMapper(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..mapper..").should()
			.haveSimpleNameEndingWith("AppMapper").check(javaClasses);
	}

	@ArchTest
	public void files_application_dto_folder_should_ending_with_Dto(
		JavaClasses javaClasses) {
		ArchRuleDefinition.classes().that().resideInAPackage("..dto..").should()
			.haveSimpleNameContaining("Dto").check(javaClasses);
	}

}
